
local physics = require("physics");
local widget = require( "widget" )
physics.start();
physics.setGravity( 0, 0 );

math.randomseed( os.time() )

local composer = require( "composer" )
local NIVEL_INICIAL = 3;

local wW = display.contentWidth;
local wH = display.contentHeight;
local scrollSpeed = 0;
display.setStatusBar( display.HiddenStatusBar )
local wC = display.contentWidth*.5;
local pontuacao = 0;
local gameStatus=1;
local pista = display.newImageRect("images/pista.jpg", 640, 480);
local pista2 = display.newImageRect("images/pista.jpg", 640, 480);
local car;
local obstacles = {};
local emitter;
local btnRestart;
local btnIniciar;
local textPontos;
local textNivel;

pista.height = wH+20;
pista.anchorX = .5;
pista.anchorY = 0;
pista.x = wW * .5;
pista.y = 0;
pista2.height = wH+20;
pista2.anchorX = .5;
pista2.anchorY = 0;
pista2.x = wW * 0.5;
pista2.y = - pista.height;

local function scrollpista(event)
    if(gameStatus == 2) then
        pista.y = pista.y + scrollSpeed
        pista2.y = pista2.y + scrollSpeed
        if (pista.y) > wH then
            pista.y = - pista.height
        end
        if (pista2.y) > wH then
            pista2.y = - pista2.height 
        end
    end
end



local function  setupExplosion()
    local dx = 40
    local p = "images/fire.png"
    local emitterParams = {
            startParticleSizeVariance = dx/2,
            startColorAlpha = 0.2,
            startColorGreen = 0.4,
            startColorRed = 1,
            yCoordFlipped = 0,
            blendFuncSource = 770,
            blendFuncDestination = 1,
            rotatePerSecondVariance = 153.95,
            particleLifespan = 0.7237,
            tangentialAcceleration = -144.74,
            startParticleSize = dx,
            textureFileName = p,
            startColorVarianceAlpha = 1,
            maxParticles = 128,
            finishParticleSize = dx/3,
            duration = 1.3,
            finishColorRed = 1,
            finishColorAlpha = 0.75,
            finishColorBlue = 0.3699196,
            finishColorGreen = 0.5443883,
            maxRadiusVariance = 172.63,
            finishParticleSizeVariance = dx/2,
            gravityy = 220.0,
            speedVariance = 258.79,
            tangentialAccelVariance = -92.11,
            angleVariance = -300.0,
            angle = -900.11
        }
        emitter = display.newEmitter(emitterParams)
        emitter:stop()
end

local function explosion()
    emitter.x = car.x
    emitter.y = car.y
    emitter:start()
end
  
local function updatePontosNivel()
    pontuacao = pontuacao+1;
    textPontos.text = "Pontos: " .. pontuacao;
    if(pontuacao%10 == 0) then
        scrollSpeed = scrollSpeed+1;
    end
    textNivel.text = "Nível: " .. scrollSpeed-3;
end


local function generateObstacles()
    local positions = {-70, 70};
    local pos = math.random(1,2);
    local position = wC + positions[pos];
    
    local obst = display.newImageRect("images/car"..(pos+1)..".png", 50, 80);
    obst.x = position;
    physics.addBody( obst, "dynamic", { isSensor=true } )
    obst.myName = "obstacle";
    table.insert(obstacles, obst);
    print(#obstacles, obst)
    return transition.to( obst, { x=obst.x+(pos * 10), y=wH+obst.height, time=1800 - (math.random(30,60)*scrollSpeed),
        onComplete = function() 
            display.remove( obst ); 
            updatePontosNivel(); 
        end
    } )
end

Runtime:addEventListener( "enterFrame", scrollpista )

function gameLoop( event )
    if(gameStatus==3) then
        transition.pause();
        transition.to( btnRestart, { x=btnRestart.x, y=display.contentCenterY, time=600})
    elseif(gameStatus==2) then
        generateObstacles();
    end
end

local timerGame = timer.performWithDelay( 600, gameLoop, 0 )

local function cleanObstacles()
    for i = #obstacles,1,-1 do
        print("remove ", obstacles[i])
        display.remove(obstacles[i]);
        table.remove(obstacles,i)
    end
end


local function initGame()
    cleanObstacles();
    transition.resume();
    pontuacao = -1;
    scrollSpeed = NIVEL_INICIAL;
    updatePontosNivel();
    car.x = wC - 75;
    car.y = wH + 80;
    if(btnRestart.y == display.contentCenterY) then
        transition.to( btnRestart, { x=btnRestart.x, y=-100, time=900 })    
    end
    transition.to( car, { x= wC - 75, y=wH - 80, time=900,
        onComplete = function() display.remove( obst ); end
    })
    gameStatus = 1;
end

local function handleButtonRestart( event )
    if ( "ended" == event.phase ) then
        initGame();
        gameStatus=2;
    end
end

local function setupImages()
    -- Car
    car = display.newImageRect("images/car1.png", 50, 80)
    car.x = wC - 75;
    car.y = wH - 80;
    car.myName = "car";
    physics.addBody( car, { radius=30, isSensor=true } );
    -- TEXTS
    textPontos = display.newText( "Pontos: " .. pontuacao, 35, 20, native.systemFont, 16 )
    textPontos:setFillColor( 255, 255, 0 )
    textPontos.anchorX = 0;

    textNivel = display.newText( "Nível: " .. scrollSpeed-3, 35, 20, native.systemFont, 16 )
    textNivel:setFillColor( 255, 255, 0 )
    textNivel.x = wW-30;
    textNivel.anchorX = 1;

    -- Buttons
    btnRestart = widget.newButton(
        {
            label = "Reiniciar",
            emboss = false,
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 160,
            height = 40,
            cornerRadius = 40,
            top=0,
            left=0,
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            fontSize= 25,
            fillColor = { default={0.9,0.9,0.34,0.9}, over={1,0.1, 0.7,0.4} },
            onEvent = handleButtonRestart
        }
    )
    btnRestart.x = display.contentCenterX
    btnRestart.y = -100;
    
    btnIniciar = widget.newButton(
        {
            label = "Iniciar",
            emboss = false,
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 160,
            height = 40,
            cornerRadius = 20,
            fillColor = { default={0.2,0.9,0.34,0.9}, over={1,0.1, 0.7,0.4} , color={1,1,1,1}},
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            fontSize= 25,
            strokeWidth = 4,
            onEvent = function()
                transition.to( btnIniciar, { x=-100, y=btnIniciar.y, time=600})
                gameStatus = 2;
                end
        }
    )
    btnIniciar.x = wW+300;
    btnIniciar.y = display.contentCenterY;
    transition.to( btnIniciar, { x=display.contentCenterX, y=btnIniciar.y, time=600})
end

-- Called when a key event has been received
local function onKeyEvent( event )
    if(event.keyName == 'right' and gameStatus == 2) then
        transition.to( car, { x=wC + 75, y=car.y, time=400 - (20 * (scrollSpeed * .04)),
            onComplete = function() display.remove( obst ); end
        })
    end
    if(event.keyName == 'left' and gameStatus == 2) then
        transition.to( car, { x= wC - 75, y=car.y, time=400 - (20 * (scrollSpeed * .04)),
            onComplete = function() display.remove( obst ); end
        })
    end
    return false
end

local function onCollision( event )
    if ( event.phase == "began" ) then
 
        local obj1 = event.object1
        local obj2 = event.object2

        if ( (obj1.myName == "car" and obj2.myName == "obstacle")
         or (obj1.myName == "obstacle" and obj2.myName == "car")) 
        then
            explosion()
            gameStatus = 3;
        end
    end
end

Runtime:addEventListener( "collision", onCollision )

setupExplosion()
setupImages()
initGame()
 
-- Add the key event listener
Runtime:addEventListener( "key", onKeyEvent )
